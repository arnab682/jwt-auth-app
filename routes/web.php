<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/{name}/{age}/{city}', function ($name,$age,$city = null){ //'/{name}/{age}[/{city}]'
    return $name.$age.$city;
});

$router->post('/{name}', function($name){
    
    return response($name);
});

$router->post('/subject/{subject}', function ($subject){ 
    return response($subject)
              ->header('Content-Type', 'application/json')
              ->header('name','Arnab')
              ->header('age','24')
              ->header('city','ctg')
              ->header('username','arnab');
});

$router->get('/foobar', function(){
    $foobar = ['foo' => 0, 'bar' => 'baz'];
    return response()->json($foobar);
});

$router->get('/first', 'TestController@first');

$router->get('/second', 'TestController@second'); 

$router->get('/download', function(){
    $path = 'demo.txt';
    //return $path;
    return response()->download($path);
});



//.........................jwt oauth...........................
// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
   // Matches "/api/register
   $router->post('register', 'AuthController@register');

   // Matches "/api/login
   $router->post('login', 'AuthController@login');

   // Matches "/api/profile
   $router->get('profile', 'UserController@profile'); 

   // Matches "/api/users/1 
   //get one user by id
   $router->get('users/{id}', 'UserController@singleUser');

   // Matches "/api/users
   $router->get('users', 'UserController@allUsers');
    
});

$router->group(['prefix' => 'log'], function () use ($router) {
    $router->get('/index', 'LogController@index');
    $router->post('/create', 'LogController@store');
    $router->get('/show/{id}', 'LogController@show');
    $router->get('/edit/{id}', 'LogController@edit');
    $router->put('/update/{id}', 'LogController@update');
    $router->delete('/delete/{id}', 'LogController@destroy');
});