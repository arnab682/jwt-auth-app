<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table ='logs';


    protected $fillable = [
        'id',
        'title',
        'created_at',
        'updated_at',

    ];


}
