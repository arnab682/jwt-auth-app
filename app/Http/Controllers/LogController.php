<?php

namespace App\Http\Controllers;


use App\Log;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LogController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $log = Log::all();
        return response()->json($log);
    }

    public function store(Request $request)
    {
        try {
//            $rules = array(
//                "name" => 'required',
//                "rfid" => 'required',
//                "image" => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
//                "operator_type" => 'required',
//            );
//            $validator = Validator::make($request->all(), $rules);
//            if ($validator->fails()) {
//                $json = [
//                    'success' => false,
//                    'errors' => $validator->messages()
//                ];
//                return response()->json($json, 400);
//            }

            $data = $request->all();

            $log = Log::create($data);
            return response()->json($log);
        }catch (QueryException $e){
            return response()->json($e->getMessage(), 500);
        }
    }

    public function show($id)
    {
        $log = Log::find($id);
        return response()->json($log);
    }


    public function edit($id){
        $log = Log::find($id);
        return response()->json($log);
    }

    public function update(Request $request,$id)
    {
        try{
//            $rules = array(
//                "name" => 'required',
//                "rfid" => 'required',
//                "image" => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
//                "operator_type" => 'required',
//            );
//            $validator = Validator::make($request->all(), $rules);
//            if ($validator->fails()) {
//                $json = [
//                    'success' => false,
//                    'errors' => $validator->messages()
//                ];
//                return response()->json($json, 400);
//            }

            $data = $request->all();
            $log = Log::find($id);
            $log->update($data);
            return response()->json($log);
        }catch (QueryException $e){
            return response()->json($e->getMessage(), 500);
        }
    }
    public function destroy($id)
    {
        try{
            $log = Log::find($id);
            $log->delete();
            return response()->json($log);
        }catch (QueryException $e){
            return response()->json($e->getMessage(), 500);
        }
    }

}
