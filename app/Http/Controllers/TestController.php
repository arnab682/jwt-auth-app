<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function first(){
        return redirect('/second');
    }

    public function second(){
        return "Second";
    }
    
}